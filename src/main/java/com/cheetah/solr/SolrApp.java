package com.cheetah.solr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

@SpringBootApplication
@EnableSolrRepositories
public class SolrApp {
    public static void main(String[] args) {
        SpringApplication.run(SolrApp.class, args);
    }
}
